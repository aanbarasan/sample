package Repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class MyDB {

	private static Connection con;
	private static Statement stmt;
	private static HashMap<String, Statement> statementList = new HashMap<>(); 
	
	public static Statement getStatement() throws Exception {
		
		if (stmt != null) {
			Class.forName("com.mysql.jdbc.Driver");
			// con = DriverManager.getConnection("jdbc:mysql://localhost:3306/urja_core", "urja", "r3n3w@bl3");
			con = DriverManager.getConnection("jdbc:mysql://192.168.23.104:3306/urja_core", "urja", "r3n3w@bl3");
			stmt = con.createStatement();
		}

		return stmt;
	}

	public static Statement getCustomStatement(String url, String userName, String password) throws Exception {
		
		Statement statement = statementList.get(url+":"+userName+":"+password+":");
		if (statement == null) {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(url, userName, password);
			statement = connection.createStatement();
			statementList.put(url+":"+userName+":"+password+":", statement);
		}

		return statement;
	}

	public static Statement getLocalDb() throws Exception {
		
		return getCustomStatement("jdbc:mysql://localhost:3306/newappdatabase", "urja","r3n3w@bl3");
	}
	
	public static Statement getCustomOneTime(String url, String userName, String password) throws Exception {
//		Class.forName("com.mysql.jdbc.Driver");
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection connection = DriverManager.getConnection(url, userName, password);
		Statement statement = connection.createStatement();
		
		return statement;
	}
}
