package basics;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApplyRegexThroughMultipleFiles {

	
	public static void main(String arg[]) throws Exception {

		String dir = "/opt/templates/templates/src/main/java/urjanet/pull/template";
		File folder = new File(dir);
		File[] listOfFiles = folder.listFiles();
		Set<String> resultset = new HashSet<String>();
		List<Object[]> counting = new ArrayList<Object[]>();
		
		
		for (int i = 0; i < listOfFiles.length; i++) {//listOfFiles.length
			if (listOfFiles[i].isFile()) {
				int count =0;
				//System.out.println("File " + listOfFiles[i].getName());
				FileInputStream ins = new FileInputStream(listOfFiles[i]);
				String text = readFile(ins);

				String regEx = "setFormatHint\\s{0,10}\\(\\s{0,10}\\\"([^\\\"]*)\\\"\\s{0,10}\\)";
				Pattern p = Pattern.compile(regEx);
				Matcher matcher = p.matcher(text);
				while(matcher.find()) {
					String result = matcher.group(1);
					resultset.add(result);
					count++;
				}
//				System.out.println(count+" "+listOfFiles[i].getName());
				final String name = listOfFiles[i].getName();
				counting.add(new Object[]{name, count});
				
			} else if (listOfFiles[i].isDirectory()) {
				//System.out.println("Directory " + listOfFiles[i].getName());
			}
		}
		
		System.out.println("===============================================");
		for(int i=1;i<counting.size();i++){
			for(int j=0;j<i;j++){	
				if((int)counting.get(i)[1] > (int)counting.get(j)[1]){
					Object[] temp = counting.get(i);
					counting.set(i,  counting.get(j));
					counting.set(j,  temp);
				}
			}
		}
		for(int i=0;i<counting.size();i++){
			System.out.println(counting.get(i)[1].toString() + "   " + counting.get(i)[0].toString());
		}
//		for(String result:resultset){
//			System.out.println(result);
//		}

	}

	private static String readFile(InputStream imp) throws IOException {
		BufferedReader buf = new BufferedReader(new InputStreamReader(imp));
		String line = buf.readLine();
		StringBuilder sb = new StringBuilder();
		while (line != null) {
			sb.append(line).append("\n");
			line = buf.readLine();
		}
		String fileAsString = sb.toString();
		return fileAsString;
	}
	
}
