package basics;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckWootForTwoJarPackage {

	public static long milliseconds = System.currentTimeMillis();

	public static void main(String arg[]) throws Exception {

		//CompareWootUsingDifferentPackages("PACEGlobal_10707-PubSvcEnterGroup_1", "/opt/Billlll/PublicServiceEnterpriseGroupTemplateProvider/Examplars/PACEGlobal_10707-PubSvcEnterGroup_1/");

		wootGeneration();
	}

	public static void CompareWootUsingDifferentPackages() throws Exception {

		List<Map<String, String>> data = new ArrayList<Map<String, String>>();

		// data.add(new HashMap<String, String>() {
		// {
		// put("accesschannel", "Goby_15765-CityOfNapervilleIL_1");
		// put("location",
		// "/opt/TV/EXT-157175/Goby_15765-CityOfNapervilleIL_1/deposit/converted");
		// }
		// });
		data.add(new HashMap<String, String>() {
			{
				put("accesschannel", "PACEGlobal_10707-PubSvcEnterGroup_1");
				put("location",
						"/opt/Billlll/PublicServiceEnterpriseGroupTemplateProvider/Examplars/PACEGlobal_10707-PubSvcEnterGroup_1/");
			}
		});
		CompareWootUsingDifferentPackages(data);
	}

	public static void CompareWootUsingDifferentPackages(List<Map<String, String>> data) throws Exception {

		for (int i = 0; i < data.size(); i++) {

			StringBuilder argumentsBuilder = new StringBuilder();
			argumentsBuilder.append(" -c ").append(data.get(i).get("accesschannel"));
			argumentsBuilder.append(" -i source_directory=").append(data.get(i).get("location"));
			argumentsBuilder.append(" -d ").append(data.get(i).get("location"));
			argumentsBuilder.append(" -i acquisition_type=LOCAL_FILE");
			argumentsBuilder.append(" --acquisitionType=LOCAL_FILE");

			String folderName = data.get(i).get("accesschannel") + "_" + milliseconds;

			process("old", argumentsBuilder.toString(), folderName, 0);

			process("new", argumentsBuilder.toString(), folderName, 0);

			compare("old", "new", folderName);
		}
	}

	public static void CompareWootUsingDifferentPackages(final String accessChannel, final String Location)
			throws Exception {

		List<Map<String, String>> data = new ArrayList<Map<String, String>>();

		data.add(new HashMap<String, String>() {
			{
				put("accesschannel", accessChannel);
				put("location", Location);
			}
		});
		CompareWootUsingDifferentPackages(data);
	}

	public static void compare(String old, String newp, String output) throws Exception {
		String dirPathOld = "/opt/projectjars/output/" + old + "/" + output;
		String dirPathNew = "/opt/projectjars/output/" + newp + "/" + output;

		File oldwootOutput = new File(dirPathOld + "/outputwoot.txt");
		File newwootOutput = new File(dirPathNew + "/outputwoot.txt");

		BufferedReader oldwootBuff = new BufferedReader(new InputStreamReader(new FileInputStream(oldwootOutput)));
		BufferedReader newwootBuff = new BufferedReader(new InputStreamReader(new FileInputStream(newwootOutput)));

		String oldwootLine = oldwootBuff.readLine();
		String newwootLine = newwootBuff.readLine();

		boolean wootsame = false;

		if (oldwootLine != null || newwootLine != null) {
			while (true) {
				if ((oldwootLine == newwootLine) || oldwootLine.toString().equals(newwootLine)) {
					wootsame = true;
				} else {
					if (oldwootLine.contains("-> ([scope:") && newwootLine.contains("-> ([scope:")) {

					} else {
						wootsame = false;
						break;
					}
				}
				oldwootLine = oldwootBuff.readLine();
				newwootLine = newwootBuff.readLine();
				if (oldwootLine == null && newwootLine == null) {
					break;
				}
			}
			oldwootBuff.close();
			newwootBuff.close();
			if (wootsame == true) {
				System.out.println(output + " same");
				new File(dirPathOld + "/same").mkdirs();
			} else {
				System.err.println(output + " differ");
				new File(dirPathOld + "/differ").mkdirs();
			}
		} else {
			new File(dirPathOld + "/nodatafound").mkdirs();
			System.err.println(output + " no data found");
		}

	}

	public static int process(String scriptArguments, String argumentsBuilder, String output, int retry)
			throws Exception {

		System.out.println("/opt/projectjars/updaterunner.sh"+ " /opt/projectjars/" + scriptArguments + "/"+ " urjanet.think.update.UpdateRunner" + argumentsBuilder);
		
		
		 ProcessBuilder chekcking = new ProcessBuilder().command("java", "-version");
		
		ProcessBuilder updateRunnerBuilder = new ProcessBuilder("/opt/projectjars/updaterunner.sh",
				"/opt/projectjars/" + scriptArguments + "/", " urjanet.think.update.UpdateRunner " + argumentsBuilder);
//		 Map<String, String> env = updateRunnerBuilder.environment();
//		 env.put("JAVA_HOME", "/opt/java");
//		 env.put("PATH", "/opt/java/bin");

		Process updateRunner = updateRunnerBuilder.start();

		BufferedReader newreader = new BufferedReader(new InputStreamReader(updateRunner.getInputStream()));
		BufferedReader errorStream = new BufferedReader(new InputStreamReader(updateRunner.getErrorStream()));
		String dirPath = "/opt/projectjars/output/" + scriptArguments + "/" + output;
		File dirs = new File(dirPath);
		dirs.mkdirs();
		File consoleOutput = new File(dirPath + "/output.txt");
		consoleOutput.createNewFile();
		File wootOutput = new File(dirPath + "/outputwoot.txt");
		wootOutput.createNewFile();
		FileOutputStream outputstream = new FileOutputStream(consoleOutput);
		FileOutputStream wootOutputstream = new FileOutputStream(wootOutput);
		StringBuilder wootfile = new StringBuilder();
		boolean wootstart = false;
		boolean communicationlinkfailure = false;

		while (true) {
			String consoleLine = newreader.readLine();
			String err = errorStream.readLine();
			if(err != null) {
				System.out.println(err);
			}
			if (consoleLine == null) {
				break;
			}
			if (consoleLine.contains("Communications link failure")) {
				communicationlinkfailure = true;
				break;
			}
			if (consoleLine.startsWith("[woot]")) {
				wootstart = true;
				wootfile = new StringBuilder();
			}
			if (wootstart == true) {
				wootfile.append(consoleLine + "\n");
			}
			if (consoleLine.startsWith("[/woot]")) {
				wootstart = false;
			}
			outputstream.write((consoleLine + "\n").getBytes());
		}
		wootOutputstream.write(wootfile.toString().getBytes());
		wootOutputstream.close();
		outputstream.close();
		updateRunner.waitFor();
		updateRunner.destroy();

		if (communicationlinkfailure == true && retry < 3) {
			return process(scriptArguments, argumentsBuilder, output, retry + 1);
		} else {
			return updateRunner.exitValue();
		}

	}

	public static void wootGeneration() throws Exception {

		String accesschannel = "Lucid_10507-CityOfSacramentoCA_1";
		String location = "/opt/TV/EXT-294297/Lucid_10507-CityOfSacramentoCA_1/deposit/ocr/two";
		
		File ff = new File(location);

		File[] files = ff.listFiles();

		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			if (file.getName().endsWith(".pdf")) {
				System.out.println(file.getName());
				String filename = file.getName().replaceAll("\\.pdf$", "");
				
				File checkone = new File(location+"/one");
				
				if(checkone.isDirectory()) {
					FileUtils.cleanDirectory(new File(location+"/one"));
				}

				FileUtils.copyFile(file, new File(location+"/one"+"/source.pdf"));

				StringBuilder argumentsBuilder = new StringBuilder();
				argumentsBuilder.append(" -c ").append(accesschannel);
				argumentsBuilder.append(" -i source_directory=").append(location+"/one");
				argumentsBuilder.append(" -d ").append(location+"/one");
				argumentsBuilder.append(" -i acquisition_type=LOCAL_FILE");
				argumentsBuilder.append(" --acquisitionType=LOCAL_FILE");
				
				String providername = accesschannel.split("\\-")[1];
				
				process("old", argumentsBuilder.toString(), providername+"_" + i, 0);

				String dirPath = "/opt/projectjars/output/" + "old" + "/" + providername + "_" + i;
				File wootOutput = new File(dirPath + "/outputwoot.txt");
				
				if( ! new File(location+"/files/").isDirectory()) {
					new File(location+"/files/").mkdirs();
				}
				
				FileUtils.copyFile(wootOutput, new File(location+"/files/" + filename + ".txt"));

				FileUtils.copyFile(file, new File(location+"/files/" + filename + ".pdf"));

			}
		}
		FileUtils.deleteDirectory(new File(location+"/one"));

	}

}
