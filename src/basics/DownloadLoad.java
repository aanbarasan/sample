package basics;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class DownloadLoad {
	
	public static void main(String arg[]) throws Exception {
		
		ProcessBuilder updateRunnerBuilder = new ProcessBuilder("/home/anbarasan/eclipse-workspace/TestCaseWorkouts/src/downloadscript.sh","");
		//Starts UpdateRunner process
		Process updateRunner = updateRunnerBuilder.start();

		BufferedReader newreader = new BufferedReader(new InputStreamReader(updateRunner.getInputStream()));
		BufferedReader errorreader = new BufferedReader(new InputStreamReader(updateRunner.getErrorStream()));
		//Stream to retrieve the console output1
//		File consoleOutput = new File("");
		while (true) {
			String consoleLine = newreader.readLine();
			String errorline = errorreader.readLine();
			if (errorline != null) {
				System.err.println(errorline);
			}
			else {
				break;
			}
			if (consoleLine == null) {
				break;
			}
			System.out.println(consoleLine);
//			output.append(consoleLine);
//			FileUtils.writeStringToFile(consoleOutput, consoleLine+"\n", "UTF-8", true);
		}
		updateRunner.waitFor();
		updateRunner.destroy();
		int exitValue =  updateRunner.exitValue();
	}
}
