package basics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author asikalisahulhameed
 *
 */
public class FindTheUniqueAlert {
	
	public static String reverse(String s) {
		
		if(s.length() == 0)
			return "";
		else 
			return s.charAt(s.length()-1)+reverse(s.substring(0, s.length()-1));
		
	}
	
 public static void main(String a[]) throws Exception {
	 
	 BufferedReader reader = new BufferedReader( new FileReader( new File( "/opt/Tickets/NorthernVirginiaElectricCooperativeTemplateProvider/NorthernVirginiaElectricCooperativeTemplateProvider.txt" )));
	
	 String o = "urjanet";
	 String p ="";
	 
	 System.err.println(reverse(o));	 
	 /* List  l = new ArrayList<>();
	 l.add(new String("asdf"));
	 l.add(new Integer(1));
	 System.err.println(l);
	 Iterator<Object> o = l.iterator();
	 while(o.hasNext())
	 {
		 Object p = o.next();
		 if(p.equals(new Integer(1)))
		 {
			 o.remove();
		 }
	 }
	 System.err.println(l);*/
	 
	 /*for(Object obj : l) {
		 if(obj.equals(new Integer(1))) {
			 l.remove(obj);
		 }
	 }*/
	 
	 Map<String, String> map = new HashMap<>();
	 
	 String line;
	 
	 int a1=0;
	 
	 while ((line = reader.readLine()) != null) {
		 
		 Matcher m = Pattern.compile( "\\[ALERT\\]\\:(.*)Charge Amount \\: \\d+" ).matcher(line);
		 
//		 Matcher m = Pattern.compile( "\\[ALERT\\]\\:(.*)\\)\\:" ).matcher(line);
		 
//		 Matcher m = Pattern.compile( "\\[ALERT\\]\\:(.*)\\s\\d+" ).matcher(line);
		 
		 if(m.find()) {
			 
			 String str = m.group();
			 if(!map.containsKey(str)){
				map.put(str, line); 
				a1++;
			}
		 }
		 
		 
	 }
	 
	 System.out.println(a1);
	 Collection<String> val = map.values();
	 for(String g: val)
	 	 System.out.println(g);
	 
	 reader.close();
 }

}
