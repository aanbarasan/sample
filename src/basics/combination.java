package basics;

import java.util.*;

public class combination {

	public static void main(String arg[]) {
		
		String[] input = new String[] { "10101", 
										"01110", 
										"00011", 
										"10100", 
										"10100" };
		
		Map<List<Integer>, Integer> map = new HashMap<>();
		
		for(int i=0;i<input.length;i++) {
			for(int j=i+1;j<input.length;j++) {
				List<Integer> list = new ArrayList<>();
				list.add(i);
				list.add(j);
				int value = 0;
				char[] str =input[i].toCharArray();
				for(int k=0;k<str.length;k++) {
					if(input[j].charAt(k) == '1' || str[k] == '1') {
						value++;
					}
				}
				map.put(list, value);
			}
		}
		
		Collection<Integer> values = map.values();
		Integer max = Collections.max(values);
		
		System.out.println(max);
		int count = 0;
		for(List<Integer> key : map.keySet()) {
			if(map.get(key).equals(max)) {
				count++;
			}
		}
		
		System.out.println(count);
	}
	
}
