package basics;

import java.util.Stack;

public class patterncheck {

	public static void main(String arg[]) {
		
		String str = "({[";
		
		Stack<Character> stk = new Stack<Character>();
		
		char[] arr = str.toCharArray();
		
		for(int i=0; i<arr.length;i++) {
			switch(arr[i]) {
				case ')':
					if(stk.lastElement().equals('(')) {
						stk.pop();
					}
					break;
				case '}':
					if(stk.lastElement().equals('{')) {
						stk.pop();
					}
					break;
				case ']':
					if(stk.lastElement().equals('[')) {
						stk.pop();
					}
					break;
				default:
					stk.push(arr[i]);
					break;
			}
		}
		System.out.println();
		while(stk.size() > 0) {
			Character ch = stk.pop();
			switch(ch) {
				case '(':
					System.out.print(")");
					break;
				case '{':
					System.out.print("}");
					break;
				case '[':
					System.out.print("]");
					break;
			}
		}
		
	}
	
}
