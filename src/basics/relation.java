package basics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class relation {

	public static void main(String arg[]) {
		
		HashMap<String,String> relate = new HashMap<String, String>(){{put("a","b"); put("b","c"); put("d","c"); put("c", "y"); }};
		String input = "y";
		List<String> inputs = new ArrayList<>();
		
		Set<Entry<String, String>> entryset = relate.entrySet();
		
		for(Entry<String, String> entry : entryset) {
			
			if(entry.getValue().equals(input)) {
				inputs.add(entry.getKey());
				
			}
			
		}
		int count = 0;
		
		while(inputs.size() > 0) {
			String str = inputs.get(0);
			
			for(Entry<String, String> entry : entryset) {
				
				if(entry.getValue().equals(str)) {
					//inputs.add(entry.getKey());
					count = count + 1;
					
				}
				
			}
			inputs.remove(0);
		}
		
		System.out.println(count);
		
	}

}
