package basics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

public class searchfiles {

	public static void main(String arg[]) {

		String location = "/opt/templates/templates/src/main/java/urjanet/pull/template/";

		try {

			File dir = new File(location);

			File[] fileList = dir.listFiles();

			for (int i = 0; i < fileList.length; i++) {
				File oneFile = fileList[i];
				//System.out.println(i + ":" + oneFile.getName());
				String printName = "";
				// Open the file
				if (oneFile.isFile()) {
					FileInputStream fstream = new FileInputStream(oneFile);
					BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

					String strLine;

					// Read File Line By Line
					while ((strLine = br.readLine()) != null) {
						Pattern p = Pattern.compile("(?i)(Click\\d*GovCX)");
						if (p.matcher(strLine).find()) {
							//System.err.println(oneFile.getName());
							printName = "\""+oneFile.getName().split("\\.")[0]+"\",";
						}
					}

					// Close the input stream
					br.close();
					fstream.close();
				}
				System.out.print(printName);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}

}
