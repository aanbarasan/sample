package basics;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

// import urjanet.utils.download.SkylightResourceDownloader;

public class skylightconnect {

	public static void main(String arg[]) {

		try {
			List<String> data = new ArrayList<>();
			if(arg.length == 0) {
				Class.forName("com.mysql.jdbc.Driver");
				// Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/urja_core", "urja", "r3n3w@bl3");
				Connection con = DriverManager.getConnection("jdbc:mysql://192.168.23.104:3306/urja_core", "urja", "r3n3w@bl3");
				
				Statement stmt = con.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT distinct ac.name, sca.sourceNodes from AccessChannel ac \n" + 
						"join StatementVersion sv on ac.id = sv.accessChannel_id \n" + 
						"join Statement st on st.defaultStatementVersion_id = sv.id \n" + 
						"join AccountData ad on ad.statementVersion_id = sv.id \n" + 
						"join StatementComponent sca on sca.id = ad.id\n" + 
						"join MeterData md on md.accountData_id = ad.id \n" + 
						"join ChargeItem ci on ci.owningStatementComponent_id = md.id\n" + 
						"where md.id not in (select owningStatementComponent_id from UsageItem)\n" + 
						"and md.id not in (select id from SanitationMeterData)\n" + 
						"and md.id not in (select id from LightingMeterData)\n" + 
						"and ci.chargeUnitsUsed is not null;");
				int cc = 0;
				while (rs.next()) {
					cc++;
					String accessChannel = rs.getString("name");
					byte[] sourceNode = (byte[]) rs.getObject("sourceNodes");
					ByteArrayInputStream in = new ByteArrayInputStream(sourceNode);
					ObjectInputStream is = new ObjectInputStream(in);
					try {
						ArrayList<String> ob = (ArrayList<String>) is.readObject();
						for(String s : ob) {
							if(!s.isEmpty() && s.contains(".") && !s.contains("html")) {
								String link = "https://skylight.urjanet.net/resources/getsource?id="+s;
								data.add(accessChannel);
								data.add(link);
								System.out.println(accessChannel);
								System.out.println(link);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				con.close();
			}
			else {
				data = Arrays.asList(arg);
			}
			
			HashMap<String, HashSet<String>> map = new HashMap<>();
			for(int i=0;i+1<data.size();i=i+2) {
				String accessChannel = data.get(i);
				String sourceNode = data.get(i+1);
				if(accessChannel.contains(".") && !accessChannel.contains("_") && !accessChannel.contains("-")) {
					System.err.println(accessChannel);
					throw new Exception();
				}
				if(!sourceNode.contains(".") && sourceNode.contains("_") && sourceNode.contains("-")) {
					System.err.println(accessChannel);
					throw new Exception();
				}
				HashSet<String> set = map.get(accessChannel);
				if(set == null) {
					set = new HashSet<>();
					map.put(accessChannel, set);
				}
				set.add(sourceNode);
			}

			for(Entry<String, HashSet<String>> entry : map.entrySet()) {
				System.err.println("++++++++++++"+entry.getKey());
//				final SkylightResourceDownloader skylightDownloader = SkylightResourceDownloader.getSkylightResourceDownloader();
//				skylightDownloader.start();
//				skylightDownloader.download(entry.getValue(), "/opt/DownloadFromMysql/subusageNotFoundForChargeHavingChargeUnitUsed/"+entry.getKey(), true);
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}

	}

}
