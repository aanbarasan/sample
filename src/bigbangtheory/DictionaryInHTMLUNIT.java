package bigbangtheory;

import java.io.IOException;
import java.net.MalformedURLException;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class DictionaryInHTMLUNIT {
	
	public static void main(String arg[]) throws Exception {
		WebClient webClient = new WebClient();
		// new String[] { "TLSv1", "TLSv1.1", "TLSv1.2", "SSLv3" }
		webClient.getOptions().setSSLClientProtocols(new String[] { "TLSv1", "TLSv1.1", "TLSv1.2", "SSLv3" });
		Page page = webClient.getPage("https://www.wordsapi.com/mashape/words/check"+"?when=2018-08-25T06:08:37.356Z&encrypted=8cfdb282e7229b9be89407bfe958beb1aeb32a0933fa91b8");
		System.out.println(page.getWebResponse().getContentAsString());
		
	}
	
}
