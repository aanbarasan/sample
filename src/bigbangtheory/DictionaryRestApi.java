package bigbangtheory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;

import Repo.MyDB;

public class DictionaryRestApi {

	private static final String USER_AGENT = "Mozilla/5.0";

	public static void main(String arg[]) throws Exception {

		// String location = "/home/anbarasan/Videos/BigBangTheorySub/Dictionary";
		String location = "/home/anbarasan/Downloads/springmvc4-xml-maven-jsp-master/src/main/webapp/resources/anbarasanmbbs.github.io/Components/Dictionary";
		
		// String url = "https://googledictionaryapi.eu-gb.mybluemix.net/?define=fuss";
		// String url = "http://services.aonaware.com/DictService/DictService.asmx/Define";
		String url = "https://www.wordsapi.com/mashape/words/";
		String param = "?when=2018-08-25T06:08:37.356Z&encrypted=8cfdb282e7229b9be89407bfe958beb1aeb32a0933fa91b8";

		Statement statement = MyDB.getCustomOneTime("jdbc:mysql://localhost:3306/newappdatabase?useSSL=false", "urja","r3n3w@bl3");
		
		ResultSet resultSet = statement.executeQuery("select * from AllWordsList where word not in (select word from BlockedWords);");

		while (resultSet.next()) {

			String text = resultSet.getString("word");
			String fileLocation = location + "/" + text + ".json";
			
			File checkExist = new File(fileLocation);
			if(checkExist.exists()) {
				continue;
			}
			String pageUrl = url+text+param;
			
			String line = getResponseFromHtmlUnit(pageUrl, text);
			if(line != null) {
				FileUtils.writeLines(new File(fileLocation), Arrays.asList(line));
				System.out.println("done word="+text);
			}
		}
	}

	public static String getResponseFromHtmlUnit(String url, String paramText) throws Exception {
		WebClient webClient = new WebClient();
		// new String[] { "TLSv1", "TLSv1.1", "TLSv1.2", "SSLv3" }
		webClient.getOptions().setSSLClientProtocols(new String[] { "TLSv1", "TLSv1.1", "TLSv1.2", "SSLv3" });
		String content = null;
		try {
			Page page = webClient.getPage(url);
			content = page.getWebResponse().getContentAsString();
		}
		catch (FailingHttpStatusCodeException e) {
			if(e.getStatusCode() == 404) {
				System.err.println(paramText);
				e.printStackTrace();
				MyDB.getLocalDb().executeUpdate("insert into BlockedWords(word) values(\""+paramText+"\");");
			}
			else {
				throw e;
			}
		}
		return content;
	}
	
	public static List<String> getResonseFromHttpClient(String url, String paramText) throws Exception{
		HttpClient client = new DefaultHttpClient();
		
		HttpPost request = new HttpPost(url);
		ArrayList<NameValuePair> postParameters;
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("word", paramText));
		request.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));

		HttpResponse response = client.execute(request);
		
		// Get the response
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		String line = "";
		List<String> lines = new ArrayList<>();
		while ((line = rd.readLine()) != null) {
			lines.add(line);
		}
		return lines;
	}
	
}
