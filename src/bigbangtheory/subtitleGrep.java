package bigbangtheory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;

import com.mysql.cj.xdevapi.JsonArray;

import Repo.MyDB;

public class subtitleGrep {

	public static void main(String arg[]) throws Exception {
		// grepFromSub();
		grepFromJson();
	}
	
	public static void grepFromJson() throws Exception {
		
		String filename = "/home/anbarasan/Downloads/springmvc4-xml-maven-jsp-master/src/main/webapp/resources/anbarasanmbbs.github.io/frequentwordmeaning/wordList.js";
		String rawcontent = FileUtils.readFileToString(new File(filename));
		String content = rawcontent.split("window.wordList = ")[1];
		
		JSONArray array = new JSONArray(content);
		for (int i = 0; i < array.length(); i++) {
			JSONObject obj = array.getJSONObject(i);
			System.out.println(obj.getString("word"));
			Statement stmt = MyDB.getCustomStatement("jdbc:mysql://localhost:3306/newappdatabase", "urja", "r3n3w@bl3");
			try {
				stmt.executeUpdate("insert into AllWordsList(word, hide) values(\""+obj.getString("word")+"\", 0);");
				System.out.println(obj.getString("word"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		System.out.println();
	}
	
	public static void grepFromSub() throws Exception {

		String location = "/home/anbarasan/Videos/BigBangTheorySub/AllSub";
		Pattern validTextLine = Pattern.compile("[A-Za-z]");
		BufferedReader reader;
		File dir = new File(location);
		File[] files = dir.listFiles();
		Map<String, Integer> map = new HashMap<>();

		for (File file : files) {
			try {
				System.out.println(file.getName());
				reader = new BufferedReader(new FileReader(file));
				String line;
				int a1 = 0;
				while ((line = reader.readLine()) != null) {
					String text = line;
					text = text.replaceAll("\\{\\\\.*?\\}", "");
					text = text.replaceAll("\\<\\/?.*?\\>", "");
					Matcher matcher = validTextLine.matcher(text);
					if (matcher.find()) {
						String[] words = text.split("\\s");
						for (String word : words) {
							String str = word.replaceAll("[^A-Za-z\\'\\-]", "").toLowerCase();
							str = str.replaceAll("(^(\\'|\\-))|((\\'|\\-)$)", "");
							if (!StringUtils.isBlank(str)) {
								Integer count = map.get(str);
								if (count == null) {
									count = 0;
								}
								count = count + 1;
								if(str.equals("abadthing")) {
									System.out.println();
								}
								map.put(str, count);
							}
						}
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		System.exit(0);
		
		ArrayList<Integer> values = new ArrayList<>(map.values());
		Collections.sort(values);
		for (int i = values.size() - 1; i >= 0; i--) {
			Iterator iterate = map.entrySet().iterator();
			while (iterate.hasNext()) {
				Map.Entry<String, Integer> entry = (Entry<String, Integer>) iterate.next();
				if (entry.getValue().equals(values.get(i))) {
					System.out.println(entry.getKey() + " - " + values.get(i));
					Statement stmt = MyDB.getCustomStatement("jdbc:mysql://localhost:3306/newappdatabase", "urja", "r3n3w@bl3");
					stmt.executeUpdate("insert into BigBangTheroy(word, count) values(\""+entry.getKey()+"\", "+entry.getValue()+");");
					iterate.remove();
					break;
				}
			}
		}
	}
}
