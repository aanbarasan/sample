package flipkart;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ProductItem")
public class ProductItem {
	
	@Id
	private long id ;
	
	@Column(name = "name", length = 1000, columnDefinition = "TEXT")
	private String name;

	@Column(name = "sizeChart")
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<SizeChart> sizeChart;

	public ProductItem(String name, List<SizeChart> sizeChart) {
		this.id = UUID.nameUUIDFromBytes(name.getBytes()).getMostSignificantBits();
		this.name = name;
		this.sizeChart = sizeChart;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SizeChart> getSizeChart() {
		return sizeChart;
	}

	public void setSizeChart(List<SizeChart> sizeChart) {
		this.sizeChart = sizeChart;
	}
}
