package flipkart;

import java.util.Arrays;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

@Entity
@Table(name = "SizeChart")
public class SizeChart {

	@Id
	private long id;

	@Column(name = "Size")
	private int size;

	@Column(name = "BrandSize")
	private int brandSize;

	@Column(name = "Waist")
	private float waist;

	@Column(name = "InseamLength")
	private float inseamLength;

	@Column(name = "Length")
	private float length;

	@Column(name = "Hip")
	private float hip;

	@Column(name = "Rise")
	private float rise;

	public SizeChart(int size, int brandSize, float waist, float inseamLength, float length, float hip, float rise) {
		String str = StringUtils.join(Arrays.asList(size, brandSize, waist, inseamLength, length, hip, rise), ",");
		this.id = UUID.nameUUIDFromBytes(str.getBytes()).getMostSignificantBits();
		this.size = size;
		this.brandSize = brandSize;
		this.waist = waist;
		this.inseamLength = inseamLength;
		this.length = length;
		this.hip = hip;
		this.rise = rise;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public int getBrandSize() {
		return brandSize;
	}

	public void setBrandSize(int brandSize) {
		this.brandSize = brandSize;
	}

	public float getWaist() {
		return waist;
	}

	public void setWaist(float waist) {
		this.waist = waist;
	}

	public float getInseamLength() {
		return inseamLength;
	}

	public void setInseamLength(float inseamLength) {
		this.inseamLength = inseamLength;
	}

	public float getLength() {
		return length;
	}

	public void setLength(float length) {
		this.length = length;
	}

	public float getHip() {
		return hip;
	}

	public void setHip(float hip) {
		this.hip = hip;
	}

	public float getRise() {
		return rise;
	}

	public void setRise(float rise) {
		this.rise = rise;
	}
}