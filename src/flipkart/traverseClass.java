package flipkart;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

public class traverseClass {

	public static SessionFactory factory = null;

	public static void main(String arg[]) throws Exception {
		try {
			intializeFactory();
			traverseClass cl = new traverseClass();
			cl.crawlPage();
			// storeByHibernate(new ProductItem("checking",
			// getObjectFromString(dummy.input)));

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			discardFactory();
		}
	}

	public static void intializeFactory() {
		System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
		Configuration configuration = new Configuration().configure("flipkart/hibernate.cfg.xml");
		factory = configuration.buildSessionFactory();
	}

	private static void discardFactory() {
		factory.close();
	}

	private void crawlPage() throws Exception {

		System.out.println("start");
		String currentFolder = "/home/anbarasan/Videos/flipkart/" + new Date().toString();

		WebClient webClient = new WebClient();
		// new String[] { "TLSv1", "TLSv1.1", "TLSv1.2", "SSLv3" }
		webClient.getOptions().setSSLClientProtocols(new String[] { "TLSv1", "TLSv1.1", "TLSv1.2", "SSLv3" });
		HtmlPage page = webClient.getPage("https://www.flipkart.com");
		savePageInThread(page, currentFolder, "homePage");
		HtmlElement element1 = (HtmlElement) page.getFirstByXPath("//a[contains(@href,'men/jeans')]");
		HtmlPage pageLoop = element1.click();

		int pageSize = 1;
		pageBreak: while (pageLoop != null) {
			String pageName = "page_" + pageSize;
			savePageInThread(pageLoop, currentFolder, pageName);
			List<HtmlElement> elementlist = pageLoop.getByXPath("//div[contains(@style,'width:25%')]/descendant::a[1]");
			for (int i = 0; i < elementlist.size(); i++) {
				if(pageSize < 8 || pageSize > 9) {
					continue;
				}
				processItem(elementlist.get(i), pageSize, currentFolder, i);
			}
			HtmlElement nextPageElement = pageLoop.getFirstByXPath("//span[contains(text(),'Next')]/ancestor::a");
			if (nextPageElement != null) {
				pageLoop = nextPageElement.click();
			} else {
				pageLoop = null;
			}
			pageSize++;
		}
	}

	public void processItem(HtmlElement htmlElement, int pageSize, String currentFolder, int i) throws IOException {
		HtmlPage pageItem = htmlElement.click();
		String itemName = "item" + pageSize + "_" + i;
		savePageInThread(pageItem, currentFolder + "/eachItem", itemName);
		String contentString = pageItem.getWebResponse().getContentAsString();
		try {
			String jsonString = contentString.split("\\_+INITIAL\\_STATE\\_+\\s+\\=\\s+")[1];
			jsonString = jsonString.split("\\;\\n")[0];
			System.out.println(jsonString);
			List<SizeChart> sizeChart = getObjectFromString(jsonString);
			ProductItem item = new ProductItem(pageItem.getBaseURL().toString(), sizeChart);
			storeByHibernate(item);
		} catch (Exception e) {
			e.printStackTrace();
		}
		pageItem.cleanUp();
	}
	
	public void savePageInThread(HtmlPage page, String folderName, String fileName) throws IOException {
		System.out.println(fileName);
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				try {
//					page.save(new File(folderName + "/" + fileName));
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}).start();
	}

	public static List<SizeChart> getObjectFromString(String jsonString) throws Exception {

		JSONObject obj = new JSONObject(jsonString);
		JSONArray headerObject = (JSONArray) grepFromJsonObject(obj, "sizeChartInfo", "headers");
		JSONArray rowsObject = (JSONArray) grepFromJsonObject(obj, "sizeChartInfo", "rows");
		return insertIntoDataBase(headerObject, rowsObject);
	}

	private static List<SizeChart> insertIntoDataBase(JSONArray headerObject, JSONArray rowsObject) throws Exception {

		List<String> headers = getHeaders(headerObject);
		List<SizeChart> sizeChart = new ArrayList<>();
		matchString(headerList, headers);
		for (int i = 0; i < rowsObject.length(); i++) {
			JSONObject obj = rowsObject.getJSONObject(i);
			JSONArray cellArray = obj.getJSONArray("cells");
			SizeChart chart = generateChart(headers, cellArray);
			sizeChart.add(chart);
		}
		return sizeChart;
	}
	
	public static List<String> headerList = Arrays.asList("Size", "Brand Size", "Waist", "Inseam Length", "Length", "Hip", "Rise");
	
	public static SizeChart generateChart(List<String> headers, JSONArray cellArray) throws Exception {
		int size = Integer.parseInt(getCellString(cellArray, headers, headerList.get(0)));
		int brandSize = Integer.parseInt(getCellString(cellArray, headers, headerList.get(1)));
		float waist = Float.parseFloat(getCellString(cellArray, headers, headerList.get(2)));
		float inseamLength = Float.parseFloat(getCellString(cellArray, headers, headerList.get(3)));
		float length = Float.parseFloat(getCellString(cellArray, headers, headerList.get(4)));
		float hip = Float.parseFloat(getCellString(cellArray, headers, headerList.get(5)));
		float rise = Float.parseFloat(getCellString(cellArray, headers, headerList.get(6)));
		SizeChart chart = new SizeChart(size, brandSize, waist, inseamLength, length, hip, rise);
		return chart;
	}

	private static void matchString(List<String> headerList, List<String> headers) {
		try {
			for(String head : headers) {
				if(!headerList.contains(head)) {
					throw new Exception(head + " header not found");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getCellString(JSONArray cellArray, List<String> headers, String head) {
		try {
			int index = headers.indexOf(head);
			if(index > 0) {
				String str = ((JSONObject) cellArray.get(headers.indexOf(head))).get("value").toString();
				if(str.contains("-")) {
					String from = str.split("-")[0];
					String to = str.split("-")[0];
					float f = Float.parseFloat(from) - Float.parseFloat(to);
					return f+"";
				}
				Pattern p = Pattern.compile("\\d+");
				if(!p.matcher(str).find()) {
					return "0";
				}
				Pattern halfPattern = Pattern.compile("\\s*1\\/2");
				Matcher halfMatcher = halfPattern.matcher(str);
				if(halfMatcher.find()) {
					str = str.replaceFirst("\\s*1\\/2", ".5");
				}
				return str;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return "0";
	}
	
	
	
	public static void storeByHibernate(ProductItem item) {
		Session session = factory.openSession();
		Transaction t = session.beginTransaction();
		session.saveOrUpdate(item);
		t.commit();
		session.close();
	}

	public static List<String> getHeaders(JSONArray headerObject) throws JSONException {
		List<String> headers = new ArrayList<>();

		for (int i = 0; i < headerObject.length(); i++) {
			JSONObject obj = (JSONObject) headerObject.get(i);
			String head = obj.getString("headerName");
			headers.add(head);
		}
		return headers;
	}

	public static Object grepFromJsonObject(Object obj, String... headers) throws JSONException {

		return grepFromJsonObject(obj, 0, headers);
	}

	public static Object grepFromJsonObject(Object obj, int current, String... headers) throws JSONException {

		if (obj == null)
			return null;

		if (obj instanceof JSONObject) {
			JSONObject jsonObj = (JSONObject) obj;
			JSONArray names = jsonObj.names();
			if (names == null)
				return null;
			for (int i = 0; i < names.length(); i++) {
				if (headers[current].equals(names.get(i))) {
					if (current == headers.length - 1) {
						return jsonObj.get(names.get(i).toString());
					} else {
						Object ob = grepFromJsonObject(jsonObj.get(names.get(i).toString()), current + 1, headers);
						if (ob != null) {
							return ob;
						}
					}
				} else {
					Object ob = grepFromJsonObject(jsonObj.get(names.get(i).toString()), 0, headers);
					if (ob != null) {
						return ob;
					}
				}
			}
		} else if (obj instanceof JSONArray) {
			JSONArray jsonArray = (JSONArray) obj;
			for (int i = 0; i < jsonArray.length(); i++) {
				if (current == headers.length - 1) {
					return jsonArray.get(i);
				} else {
					Object ob = grepFromJsonObject(jsonArray.get(i), 0, headers);
					if (ob != null) {
						return ob;
					}
				}
			}
		}

		return null;
	}
}
